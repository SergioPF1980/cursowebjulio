window.addEventListener("load", () => {
    var infoButton = document.getElementById('infoButton');
    infoButton.addEventListener("click", validation);
    // infoButton.addEventListener("click", nif);

    //Foco en el primer elemento
    // document.getElementById('name').focus();
});

function validation() {
    var info_forms = document.myForm;

    var firstName = document.getElementById('myname').value.trim();
    var secondName = document.getElementById('mysecondName').value.trim();
    var city = document.getElementById('mycity').value.trim();

    if(!firstName || !secondName || !city){
        return false;
    }
    

    var email = document.getElementById('myemail').value.trim();
    if (!(/\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)/.test(email))) {
        return false;
    }

    // console.log(firstName + " - "+ secondName + " - " + city + " - " + 
    //                         dni + " - " + phone + " - " + email);

        // if (firstName == '' || firstName.length == 0 || /^\s+$/.test(firstName)) {
        //     return false;
        // }

        // if (secondName == '' || secondName.length == 0 || /^\s+$/.test(secondName)) {
        //     return false;
        // }

        // if (city == '' || city.length == 0 || /^\s+$/.test(city)) {
        //     return false;
        // }

        
        var phone = document.getElementById('myphone').value.trim();
        if (!(/^\d{9}$/.test(phone))) {
            return false;
        }

        return true;

        

        var dni = document.getElementById('mydni').value;
        var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

        if (!(/^\d{8}[A-Z]$/.test(dni))) {
            return false;
        }

        if (dni.charAt(8) != letras[(dni.substring(0, 8)) % 23]) {
            return false;
        }

}

function nif(dni) {
    var numero
    var letr
    var letra
    var expresion_regular_dni

    expresion_regular_dni = /^\d{8}[a-zA-Z]$/;

    if (expresion_regular_dni.test(dni) == true) {
        numero = dni.substr(0, dni.length - 1);
        letr = dni.substr(dni.length - 1, 1);
        numero = numero % 23;
        letra = 'TRWAGMYFPDXBNJZSQVHLCKET';
        letra = letra.substring(numero, numero + 1);
        if (letra != letr.toUpperCase()) {
            alert('Dni erroneo, la letra del NIF no se corresponde');
        } else {
            alert('Dni correcto');
        }
    } else {
        alert('Dni erroneo, formato no válido');
    }
}