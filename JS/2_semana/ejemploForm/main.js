// window.addEventListener("load", () => {
//     var infoButton = document.getElementById('myButton');
//     infoButton.addEventListener("click", validateForm);
//     // infoButton.addEventListener("click", nif);

//     //Foco en el primer elemento
//     // document.getElementById('name').focus();
// });

const nombre = document.getElementById('name');
const email = document.getElementById('email');
const pass = document.getElementById('password');
const phone = document.getElementById('myphone');
const radio = document.getElementsByName('sexo');
const color = document.getElementsByName('color');
const condiciones = document.getElementsByName('condiciones');


const form = document.getElementById('myForm');
const parrafo = document.getElementById('warnings');

form.addEventListener("submit", e => {

    e.preventDefault();
    let warnings = "";
    let nombreValidation = /^[a-zA-Z]\s*$/;
    let emailValidation = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
    let phoneValidation = /^[9|6]{1}([\d]{2}[-]*){3}[\d]{2}$/;
    // let passValidation = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,18}$;

    let entrar = false;
    parrafo.innerHTML = "";


    if (nombre.value.length < 4 && !nombreValidation.test(nombre.value)) {
        // alert('Nombre corto');
        warnings += `El nombre no es valido </br>`;
        entrar = true;
    }

    // console.log(emailValidation.test(email.value));
    if (!emailValidation.test(email.value)) {
        warnings += `El email no es valido </br>`;
        entrar = true;
    }

    if (pass.value.length < 8) {
        warnings += `El password no es valido </br>`;
        entrar = true;
    }

    if (!phoneValidation.test(phone.value)) {
        warnings += `El Teléfono no es correcto </br>`;
        entrar = true;
    }

    var radioElegido = false;
    for (let i = 0; i < radio.length; i++) {
        if (radio[i].checked == true) {
            radioElegido = true;
        }
    }

    if (radioElegido == false) {
        warnings += `Debe seleccionar un sexo </br>`;
    }


    if (color.value == 0 || color.value == "") {
        warnings += `Debe seleccionar un color </br>`;
        entrar = true;
    }

    if (condiciones.checked) {
        warnings += `Debe aceptar los terminos </br>`;
        entrar = true;
    }

    if (entrar) {
        parrafo.innerHTML = warnings;
    } else {
        parrafo.innerHTML = "Envio correcto";
    }

});