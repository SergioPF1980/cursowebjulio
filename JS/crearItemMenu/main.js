const lista = document.getElementById('lista');
// console.log(lista);



const arrayItem = ['Item 1', 'Item 2', 'Item 3'];
const fragment = document.createDocumentFragment();

for(const item of arrayItem){
    const itemList = document.createElement('li');
    itemList.textContent = item;
    fragment.appendChild(itemList);
}

lista.appendChild(fragment);