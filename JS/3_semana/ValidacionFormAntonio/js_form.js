function validateForm() {
    var errors = [];
    var name = document.getElementById('name').value.trim();
    var lastname = document.getElementById('lastname').value.trim();
    var country = document.getElementById('country').value.trim();
    /*if(!name || !lastname || !country) {
        return false;
    }*/
    if(!name) {
        errors.push('El campo Nombre es obligatorio');
    }
    if(!lastname) {
        errors.push('El campo Apellido es obligatorio');
    }
    if(!country) {
        errors.push('El campo País es obligatorio');
    }
    if(!validateDNI(document.getElementById('dni').value.trim())){
        errors.push('El DNI introducido no es válido');
        //return false;
    }
    /*
    Los símbolos arroba "@" y el punto "." son obligatorios; tanto el 
    texto "micorreo" como "servidor" pueden ser variables; el texto de 
    después del punto ("com") puede ser variable con ciertas restricciones: 
    siempre tiene que ir en minúscula, y además tener siempre dos o tres caracteres.

    Eso en una expresión regular se traduce en lo siguiente:
    /[\w]+@{1}[\w]+\.[a-z]{2,3}/
    */
    var email = document.getElementById('email').value.trim();
    if(!(/[\w]+@{1}[\w]+\.[a-z]{2,3}/.test(email))) {
        errors.push('El campo Email no tiene formato correcto');
        // return false;
    }
    /*
    Se comprueba que el número tenga 9 cifras, y las cuales pueden ponerse 
    todas seguidas o separadas por guiones en grupos: el primero de tres, y 
    los demás de dos (admite los formatos "000000000", ó 000-00-00-00, también 
    el 000-000000, pero no el 000-000-000). La expresión regular es la siguiente:
    /^[\d]{3}[-]*([\d]{2}[-]*){2}[\d]{2}$/

    Podemos ajustar más la comprobación si comprobamos también que la primera 
    cifra sea un 9 ó un 6, ya que todos los teléfonos normales en España empiezan 
    por esas cifras (fijos o móviles). La expresión regular ahora quedará así:
    /^[9|6]{1}([\d]{2}[-]*){3}[\d]{2}$/
    */
    var phone = document.getElementById('phone').value.trim();
    if(!(/^[9|6]{1}([\d]{2}[-]*){3}[\d]{2}$/.test(phone))){
        errors.push('El campo Teléfono no tiene formato correcto');
        // return false;
    }
    if(errors.length > 0) {
        showErrors(errors);
        return false;
    }
    return true;
}

function validateDNI(dni) {
    var expresion_regular_dni = /^\d{8}[a-zA-Z]$/;

    if(expresion_regular_dni.test (dni) == true){
        let numero = dni.substr(0,dni.length-1);
        let letr = dni.substr(dni.length-1,1);
        numero = numero % 23;
        let letra='TRWAGMYFPDXBNJZSQVHLCKET';
        letra=letra.substring(numero,numero+1);
        if (letra!=letr.toUpperCase()) {
            return false;
        }else{
            return true;
        }
    }else{
        return false;
    }
}

function showErrors(formErrors) {
    var errorsContent = document.getElementById('errors-content');
    errorsContent.innerHTML = ''; 
    errorsContent.className = ''; 
    for(var i in formErrors) {
        var tagP = document.createElement('p');
        tagP.innerHTML = formErrors[i]; 
        errorsContent.appendChild(tagP);
    }
    errorsContent.className = 'error'; 
}




