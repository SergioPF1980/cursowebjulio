window.addEventListener("load", init);

function init() {
    document.getElementById("country").addEventListener("change", checkCountry);
    document.formTest.addEventListener("submit", validate);
}

function checkCountry() {
    if (document.getElementById("country").value == "spa") {
        document.getElementById("showDNI").style.display = "block";
    } else {
        document.getElementById("showDNI").style.display = "none";
    }
}

function validate(event) {
    var myForm = document.formTest;
    var myErrors = [];
    for (let index = 0; index < myForm.length; index++) {
        const element = myForm[index];
        switch (element.name) {
            case "country":
                if (element.value.trim() == "") {
                    myErrors.push("Error: el campo país no puede estar vacío.")
                }
                break;
            case "name":
                if (element.value.trim() == "") {
                    myErrors.push("Error: el campo nombre no puede estar vacío.")
                }
                break;
            case "DNI":
                if (document.getElementById("country").value == "spa") {
                    if (element.value.trim() == "") {
                        myErrors.push("Error: el campo DNI no puede estar vacío.")
                    } else if (!nif(element.value)) {
                        myErrors.push("Error: el campo DNI contiene.")
                    }
                }
                break;
            default:
                break;
        }
    }

    if (myErrors.length>0) {
        alert("Se han encontrado errores (Revisar la consola)");
        for (let index = 0; index < myErrors.length; index++) {
            const element = myErrors[index];
            console.log(element);
        }
        event.preventDefault();
        return false;
    }
    return true;
}

function nif(dni) {
    var numero;
    var letr;
    var letra;
    var expresion_regular_dni;
    var err = [];

    expresion_regular_dni = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKE]$/i;

    if (expresion_regular_dni.test(dni) == true) {
        //Se separan los números de la letra
        var letraDNI = dni.substring(8, 9).toUpperCase();
        var numDNI = parseInt(dni.substring(0, 8));
        //Se calcula la letra correspondiente al número
        var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
        var letraCorrecta = letras[numDNI % 23];

        if (letraDNI != letraCorrecta) {
            return false;
        }
    } else {
        return false;
    }
    return true;
}