window.addEventListener('load', init);

function init() {
    var aItems = getAllNavItems();
    for(var i= 0; i < aItems.length; i++) {
        let aItem = aItems[i];
        aItem.addEventListener('click', () => {
            clearAllActive();
            addActiveToElement(aItem);
        });
    }
}

function getAllNavItems() {
    var navUl = document.getElementById('nav');
    return navUl.getElementsByTagName('a');
}

function addActiveToElement(item) {
    var navParent = item.parentElement;
    if(navParent.className.includes('dropdown-content')) {
        // si el padre del elemento pulsado tiene la clase "dropdown-content"
        // estoy en un submenu
        let submenuParent = navParent.parentElement;
        let firtA = submenuParent.getElementsByTagName('a')[0];
        firtA.classList.add('active');
    }
    item.classList.add('active');
}

function clearAllActive() {
    var items = getAllNavItems();
    for(var i= 0; i < items.length; i++) {
        items[i].classList.remove('active');
    }
}