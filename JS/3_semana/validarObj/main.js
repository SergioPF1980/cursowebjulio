window.addEventListener("load", () => {

    document.myForm.addEventListener("submit", validateForm);
});

function validateForm(event) {
    event.preventDefault();
    let regExpemail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
    var myForm = document.myForm;
    var objForm = {
        fav_language: requiredValue('fav_language'),
        colors: requiredValue('colors'),
        email: requiredFormat('email', regExpemail),
        comments: requiredValue('comments'),
        name: requiredValue('myName')
    };

    for (let i = 0; i < Object.keys(objForm).length; i++) {
        objForm[i]
        
    }

    Object.keys(objForm);
    // console.log(objForm);
    // console.log(objForm.fav_language);
    return false;
    
    

}

function requiredValue(field) {
    try {
        var result= {};
        var fielValue = document.myForm[field].value.trim();
        //console.log(fielValue);
        if(fielValue){
            result.validation = true;
            return result;
        }
        return{
            validation: false,
            error: 'El campo' + field + ' es obligatorio'
        };
    }catch(exceptionError){
        return{
            validation: false,
            error: exceptionError.message
        };
    }
}
