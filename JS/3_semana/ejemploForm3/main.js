window.addEventListener('load', init);

function init() {
    var form = document.getElementById('myForm');
    // var pais = document.getElementById('myPais');

    form.addEventListener('change', showDni);
    form.addEventListener('submit', validateForm);
    
}

function showDni() {
    var dni = document.getElementById("cambiar");
    var pais = document.getElementById("myPais");
    // document.getElementById('name').focus();

    if (pais.value == "España") {
        dni.style.display='block';
    }
    else {
        dni.style.display='none';
    }
}

function validateForm(event){
    event.preventDefault();
    var errors = [];

    var form = document.getElementById('myForm');

    
    // var name = document.getElementById('name').value.trim();

    // if(!name) {
    //     errors.push('El campo Nombre es obligatorio');
    // }



    if(errors.length > 0) {
        showErrors(errors);
        return false;
    }
    return true;
}

function validateDNI(dni) {
    var expresion_regular_dni = /^\d{8}[a-zA-Z]$/;

    if(expresion_regular_dni.test (dni) == true){
        let numero = dni.substr(0,dni.length-1);
        let letr = dni.substr(dni.length-1,1);
        numero = numero % 23;
        let letra='TRWAGMYFPDXBNJZSQVHLCKET';
        letra=letra.substring(numero,numero+1);
        if (letra!=letr.toUpperCase()) {
            return false;
        }else{
            return true;
        }
    }else{
        return false;
    }
}

function showErrors(formErrors) {
    var errorsContent = document.getElementById('errors-content');
    errorsContent.innerHTML = ''; 
    errorsContent.className = ''; 
    for(var i in formErrors) {
        var tagP = document.createElement('p');
        tagP.innerHTML = formErrors[i]; 
        errorsContent.appendChild(tagP);
    }
    errorsContent.className = 'error'; 
}

