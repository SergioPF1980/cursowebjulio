window.onload = () => {
    var buttons = document.getElementsByClassName("boton"); // Devuelve una array, verdad y mentira. HTMLCollections
    for (var i = 0; i < buttons.length; i++) {
        let item = buttons[i]; //Guardamos en otra variable local del for para que coja todos los valores.
        //Asi no se machaca
        // En el ejemplo del error se podría usar let para que funcionara
        item.addEventListener("click", () => changeDiv(item.id));

        // Al pasaar por el boton cambia a su color
        // item.addEventListener("mouseenter", () => changeDiv(item.id));
    }

    var box = document.getElementById('myDiv');
    box.addEventListener('mouseenter', () => {
        box.classList.add('vacio');
        console.log('estas aqui');
    });

    box.addEventListener('mouseleave', () => {
        box.classList.remove('vacio');
        console.log('Has salido');
    });

    // var box = document.getElementById('myDiv');
    // box.addEventListener('mouseenter', () => {
    //     box.style.background = "green";
    // });

    // box.addEventListener('mouseleave', () => {
    //     box.style.background = "black";
    // });

    /*PARRAFO 2*/
    var parrafo = document.getElementById('p1');

    parrafo.addEventListener('mouseover', () => {
        parrafo.style.color = "red";
    })

    parrafo.addEventListener('mouseout', () => {
        parrafo.style.color = "black";
    })

    /*PARRAFO 2*/
    var parrafo2 = document.getElementById('p2');

    parrafo2.addEventListener('mouseover', () => {
        parrafo2.style.color = "green";
    })

    /*PARRAFO 3*/
    var parrafo3 = document.getElementById('p3');

    parrafo3.addEventListener('mouseover', () => {
        parrafo3.style.color = "orange";
    })


    //SOLUCION MIGUEL
    document.getElementById("rojo")
        .addEventListener("mouseover", function() {cambiar("red", "rojo")}, false);


    function cambiar(color, id) {
        document.getElementById(id).style.color = color;
    }
}







function changeDiv(customClass = 'standard') {
    var myDiv = document.getElementById('myDiv');
    myDiv.className = customClass;
}