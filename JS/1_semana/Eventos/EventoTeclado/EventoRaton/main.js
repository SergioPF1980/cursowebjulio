window.onload = function () {			
    document.body.onmousemove = info
    document.getElementById("caja").onclick = mover;
    document.getElementById("caja").ondblclick = mover;
}

var estado = "parar" // Estado inicial del cuadrado: sin mover
function info(elEvento) {
    var evento = elEvento || window.event; //obtener objeto event
    var despX = document.documentElement.scrollLeft; //desplazamiento de la pagiina al hacer scroll
    var despY = document.documentElement.scrollTop;
    var ventanaX = evento.clientX; //coordenadas de la ventana
    var ventanaY = evento.clientY;
    var paginaX = ventanaX + despX; //coordenadas de la página
    var paginaY = ventanaY + despY;
    var pantallaX = evento.screenX //coordenadas de la pantalla
    var pantallaY = evento.screenY
    caja1 = document.getElementById("caja") //posición del cuadrado
    posx = paginaX - 50
    posy = paginaY - 50
    if (estado == "mover") { //desplazamiento del cuadrado
        caja1.style.left = posx + "px"
        caja1.style.top = posy + "px"
    }
    texto = document.getElementById("posicion") //escribir coordenadas de la página
    texto.innerHTML = "Coordenadas de la página: " + paginaX + "px, " + paginaY + "px."
    texto2 = document.getElementById("ventana") //escribir coordenadas de la ventana
    texto2.innerHTML = "Coordenadas de la ventana:  " + ventanaX + "px, " + ventanaY + "px."
    texto3 = document.getElementById("pantalla") //escribir coordenadas de la pantalla
    texto3.innerHTML = "Coordenadas de la pantalla: " + pantallaX + "px, " + pantallaY + "px."
    texto4 = document.getElementById("accion") //e3scribir estado del cuadrado
    texto4.innerHTML = "Movimiento del cuadrado: " + estado
    texto5 = document.getElementById("desplaza") //scroll de la página
    texto5.innerHTML = "Desplazamiento de la página: " + despX + ", " + despY
}

function mover(ev) { //control del movimiento del cuadrado
    var evento2 = ev || window.event //distinguir acción del ratón
    tipo = evento2.type
    if (tipo == "click") { //coger el cuadrado (un  click)
        estado = "mover"
    } else if (tipo == "dblclick") { //soltar el cuadrado (doble click)
        estado = "parar"
    }
}
