window.addEventListener("load", () => {
    document.addEventListener("mousemove", informacion);
    document.addEventListener("click", informacion);

    document.addEventListener("mousemove", position);
});

/* Puntero con el raton */

function informacion(elEvento) {
    var evento = elEvento || window.event;
    switch (evento.type) {
        case 'mousemove':
            document.getElementById('informacion').style.backgroundColor = '#fff';
            document.getElementById('informacion').style.backgroundColor = '#ccc';
            var posicionX = evento.clientX;
            var posicionY = evento.clientY;
            muestraInformacion(posicionX, posicionY);
            break;
        case 'click':
            document.getElementById('informacion').style.backgroundColor = '#3498DB';
            document.getElementById('informacion').style.color = '#fff';
            document.getElementById('informacion').innerHTML = '<h1>Has hecho click en el cuadrado</h1>'
    }
}
function muestraInformacion(coordX, coordY){
    document.getElementById('informacion').innerHTML = '<p> CoordenadaX:'+ coordX + '</p>' + '<br/> <p> CoordenaY: ' + coordY + '</p>';
}

function position(event) {
    var x = event.clientX;
    var y = event.clientY;
    var mousePositions = document.getElementById('mousePositions');
    mousePositions.innerHTML = 'X coords: ' + x + ', Y coords: ' + y;

}
