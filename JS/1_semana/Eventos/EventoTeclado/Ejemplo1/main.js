window.addEventListener("load", () => {
    document.addEventListener("keyup", showInfo);
    // document.addEventListener("keypress", showInfo);
    // document.addEventListener("keydown", showInfo);

    document.addEventListener("keydown", mover);
    document.addEventListener("onmouseover", mostrarPosicionPuntero);
});



function showInfo(myEvent) {
    var event = myEvent || window.event;
    console.log(event);
    console.log(event.type);
    console.log(event.altkey); //ALT Shift y Ctrl por separado
    console.log(event.keyCode);
    console.log(event.charCode);
    console.log(String.fromCharCode(event.charCode));
}


//     document.onkeyup = muestraInformacion;
//     document.onkeypress = muestraInformacion;
//     document.onkeydown = muestraInformacion;


// function muestraInformacion(elEvento){
//     var evento = window.event || elEvento;
//     var mensaje = "Tipo de evento: " + evento.type + "<br>" + 
//     "Propiedad keyCode: " + evento.keyCode + "<br>" +
//     "Propiedad charCode: " + evento.keyCode + "<br>" +
//     "Caracter pulsado: " + String.fromCharCode(evento.charCode);
//     inof.innerHtml += "<br>--------------<br>" + mensaje
// }

/* Crear funcion....Desplace Caja con las flechas */


var x = 0;
var y = 0;
var hijo = document.getElementById('hijo');

function mover(event) {

    switch (event.keyCode) {
        case 37: //izquierda x
            x = x - 10;
            hijo.style.left = x + "px";
            break;
        case 38: //arriba y +
            y = y + 10;
            hijo.style.top = y + "px";
            break;
        case 39: //Derecha x
            x = x + 10;
            hijo.style.left = x + "px";
            break;
        case 40: //abajo y 
            y = y - 10;
            hijo.style.top = y + "px";
            break;
        default:
            alert("Se ha equivocado, debe pulsar las flechas del teclado");
    }
}

/* Puntero con el raton */


function mostrarPosicionPuntero(eventoObj){
    var posicionX = eventoObj.clientX;
    var posicionY = eventoObj.clientY;
    var nodoCoordenadas = document.getElementById('coordenadas');
    nodoCoordenadas.innerHTML = 'Posicion x: ' + posicionX + ' - Posicion y: ' + posicionY;
}

// function informacion(elEvento){

// }
// function muestraInformacion(x, y){
    
// }
// // https://aprenderaprogramar.com/foros/index.php?topic=3495.0
// document.onmousemove = informacion;
// document.onclick = informacion;