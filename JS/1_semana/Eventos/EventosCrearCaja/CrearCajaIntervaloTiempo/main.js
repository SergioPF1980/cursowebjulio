window.addEventListener("load", () => init());
var MYVAR;

function init() {

    var btnSaludar = document.getElementById('boton');
    // btnSaludar.addEventListener("mouseout", saludar);
    // btnSaludar.addEventListener('click', crearCaja);
    btnSaludar.addEventListener('click', intervalo);

    var btnReset = document.getElementById('reset');
    btnReset.addEventListener('click', pararInterval);

}

function crearCaja() {
    var caja = document.createElement('div');
    caja.className = "caja"; //caja.style.width = "20px";
    var container = document.getElementById('container');
    container.appendChild(caja);
}

function intervalo() {
    MYVAR = setInterval(function () {
        crearCaja();
    }, 500);
}

//www.youtube.com/watch?v=JzRr-NF8EII
function pararInterval() {
    clearInterval(MYVAR);
}

// function saludar() {
//     alert('Saludar');
// }