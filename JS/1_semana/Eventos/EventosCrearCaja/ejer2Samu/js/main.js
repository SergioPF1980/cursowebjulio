window.addEventListener("load", () => init());
function init() {
    var myVar = "Hola"
    var buttons = document.getElementsByTagName("button");
    for (let index = 0; index < buttons.length; index++) {
        let element = buttons[index];
        element.addEventListener("click", () => alertar(myVar));
    }
}
function alertar(v) {
    setTimeout(() => alert(v), 3000);
}
