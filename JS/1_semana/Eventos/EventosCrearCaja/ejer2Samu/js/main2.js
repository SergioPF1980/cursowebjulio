window.addEventListener("load", () => init());
var estilo = "width: 20px; height: 20px; background-color: red; margin: 1em; display: inline-block;";
function init() {
    var buttons = document.getElementsByTagName("button");
    for (let index = 0; index < buttons.length; index++) {
        let element = buttons[index];
        element.addEventListener("click", () => addDiv(estilo));
    }
}
function addDiv(v) {
    var myDiv = document.createElement('div');
    myDiv.style=v;
    document.body.appendChild(myDiv);
}