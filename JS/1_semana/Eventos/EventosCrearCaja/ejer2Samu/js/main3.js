window.addEventListener("load", () => init());
var estilo = "width: 100px; height: 100px; margin: 1em; display: inline-block;";
var intervalo;
function init() {
    var buttons = document.getElementsByTagName("button");
    for (let index = 0; index < buttons.length; index++) {
        let element = buttons[index];
        if (element.id == "crear") {
            element.addEventListener("click", () => iniciar());
        }
        else if (element.id == "parar") {
            element.addEventListener("click", () => parar());
        }
        else
        element.addEventListener("click", () => addDiv());

    }
}
function iniciar() {
    intervalo = setInterval(() => addDiv(), 550);
}
function parar() {
    clearInterval(intervalo);
}
function addDiv() {
    var myDiv = document.createElement('div');
    myDiv.style = estilo;
    myDiv.style.backgroundColor=getRandomColor();
    document.body.appendChild(myDiv);
}
function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }