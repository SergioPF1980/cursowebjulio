window.addEventListener("load", () => init());

function init() {
    var parrafo = document.getElementById("1");
    parrafo.addEventListener("click", () => {
        cambiarParrafo(parrafo.id);
    });

    var parrafo2 = document.getElementById("2");
    parrafo2.addEventListener("click", () => {
        cambiarParrafo(parrafo2.id);
    });

    var parrafo3 = document.getElementById("3");
    parrafo3.addEventListener("click", () => {
        cambiarParrafo(parrafo3.id);
    });


    // OCULTAR Y MOSTRAR CON BOTONES 

    var btnOcultar = document.querySelector("#btnOcultar");
    // alert(btnOcultar.lenght);
    var btnMostrar = document.querySelector("#btnMostrar");
    var parr = document.querySelector("#parr");

    btnMostrar.addEventListener("click", () => {
        parr.style.display = "block";
    });

    btnOcultar.addEventListener("click", () => {
        parr.style.display = "none";
    });

    /**Boton Alert */
    var btnAlert = document.querySelector("#btnAlert");
    // alert(btnOcultar.lenght);


    btnAlert.addEventListener("click", () => {
        setTimeout(function () {
            alert("Hello, Pasaron 3 segundos");
        }, 3000);
    });

    
    

    /***********************************************************************************************************/
    // CREAR PARRAFOS, ENLACES Y LISTA
    let contenedor = document.getElementById('resultado');

    // crear parrafos
    let p1 = document.createElement('p');
    let p1Texto = document.createTextNode('parrafo creado 1');
    p1.appendChild(p1Texto);

    let p2 = document.createElement('p');
    let p2Texto = document.createTextNode('parrafo creado 2');
    p2.appendChild(p2Texto);

    let p3 = document.createElement('p');
    let p3Texto = document.createTextNode('parrafo creado 3');
    p3.appendChild(p3Texto);

    contenedor.appendChild(p1);
    contenedor.appendChild(p2);
    contenedor.appendChild(p3);

    // crear enlaces
    let link1 = document.createElement('a');
    link1.setAttribute('href', "https://www.w3schools.com/");
    let linkTexto = document.createTextNode('Enlace 3W');
    link1.appendChild(linkTexto);
    contenedor.appendChild(link1);

    // crear lista desordenadas
    let lista = document.createElement('ul');

    for (let i = 1; i < 4; i++) {
        let li = document.createElement('li');
        let liTexto = document.createTextNode('Elemento ' + i);
        li.appendChild(liTexto);
        lista.appendChild(li);
    }

    contenedor.appendChild(lista);


    // CREAR SELECT Y DATALIST

    //Select
    let select = document.createElement('select');

    let option1 = document.createElement('option');
    option1.setAttribute("value", "value1");
    let option1Text = document.createTextNode("Opcion 1");
    option1.appendChild(option1Text);

    let option2 = document.createElement('option');
    option2.setAttribute("value", "value2");
    let option2Text = document.createTextNode("Opcion 2");
    option2.appendChild(option2Text);

    let option3 = document.createElement('option');
    option3.setAttribute("value", "value3");
    let option3Text = document.createTextNode("Opcion 3");
    option3.appendChild(option3Text);

    select.appendChild(option1);
    select.appendChild(option2);
    select.appendChild(option3);

    let selectContent = document.getElementById('select-contenedor');
    selectContent.appendChild(select);

    // Datalist
    let input = document.createElement('input');
    input.setAttribute('list', 'lista');

    let datalist = document.createElement('datalist');
    datalist.setAttribute("id", "lista");

    let optioDL2 = document.createElement('option');
    optioDL2.setAttribute("value", "opcion 2");

    let optioDL1 = document.createElement('option');
    optioDL1.setAttribute("value", "opcion 1");

    let optioDL3 = document.createElement('option');
    optioDL3.setAttribute("value", "opcion 3");

    datalist.appendChild(optioDL1);
    datalist.appendChild(optioDL2);
    datalist.appendChild(optioDL3);

    let datalistContenedor = document.getElementById('datalist-contenedor');
    datalistContenedor.appendChild(input);
    datalistContenedor.appendChild(datalist);
}

function cambiarParrafo(id) {
    var enlace = document.getElementById(id);
    var parrafo = document.getElementById("parrafo" + id);

    if (parrafo.style.display == "" || parrafo.style.display == "block") {
        parrafo.style.display = "none";
        enlace.innerHTML = ">Mostrar parrafo";
    } else {
        parrafo.style.display = "block";
        enlace.innerHTML = ">Ocultar Contenido";
    }
}
/** Reloj **/

function ActivarReloj() {
    //Obteniendo datos del tiempo
    var laHora = new Date();
    var horario = laHora.getHours();
    var minutero = laHora.getMinutes();
    var segundero = laHora.getSeconds();

    if (minutero < 10)
        minutero = "0" + minutero;
    if (segundero < 10)
        segundero = "0" + segundero;

    //escribiendo sobre el campo de texto la hora actual 
    document.getElementById('sonLas').value = horario + ":" + minutero + ":" + segundero;
    //Desactivando el boton 'Activar Reloj'
    document.getElementById('botonActivar').disabled = true;
    //Actualizando la hora cada 1 segundo
    ahoraSonLas = setTimeout(ActivarReloj, 1000);
}

function DetenerReloj() {
    // Deteniendo el setTimeout
    clearTimeout(ahoraSonLas);
    // Volviendo el boton 'Activar Reloj' a la normalidad
    document.getElementById('botonActivar').disabled = false;
}