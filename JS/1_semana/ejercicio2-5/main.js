/**
 *  A partir de la página web proporcionada y utilizando las funciones DOM, mostrar
    dentro del div la siguiente información:

    Número de enlaces de la página //tAGNAMA

    Dirección a la que enlaza el penúltimo enlace

    Numero de enlaces que enlazan a http://prueba

    Número de enlaces del segundo párrafo
 */


window.onload = function () {
    var idInfor = document.getElementById("informacion");

    //Numero de enlaces
    var enlace = document.getElementsByTagName("a");
    idInfor.innerHTML = "Los enlaces son: " + enlace.length;


    // Dirección a la que enlaza el penúltimo enlace
    var mensaje = "El penultimo enlace apunta a: " + enlace[enlace.length - 2]; //  Es -2 porque hay que restarle 2, uno del .lenght y otro porque queremos el penúltimo

    idInfor.innerHTML = idInfor.innerHTML + "<br/>" + mensaje;

    //  Numero de enlaces que enlazan a http://prueba
    var cont = 0;
    for (let i = 0; i < enlace.length; i++) {
        // alert(enlace[i]);
        if (enlace[i].href == "http://prueba/") {
            cont++;
        }
        // console.log(enlace[i]);
    }

    idInfor.innerHTML = idInfor.innerHTML  + "<br/>"  + cont + " Enlaces de prueba"

    // Número de enlaces del segundo párrafo
    var parrafo = document.getElementsByTagName("p");
    enlace = parrafo[1].getElementsByTagName("a");

    idInfor.innerHTML = idInfor.innerHTML + "<br/>" + "Los enlaces son: " + enlace.length;
}