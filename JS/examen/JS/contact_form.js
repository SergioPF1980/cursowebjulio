const nombre = document.getElementById('name');
const email = document.getElementById('email');
const textarea = document.getElementById('texto');

const form = document.getElementById('myForm');
const parrafo = document.getElementById('warnings');

form.addEventListener("submit", e => {

    e.preventDefault();
    let warnings = "";
    let nombreValidation = /^[a-zA-Z]\s*$/;
    let emailValidation = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

    let entrar = false;
    parrafo.innerHTML = "";

    if (!nombreValidation.test(nombre.value)) {
        warnings += `El nombre no es valido </br>`;
        entrar = true;
    }

    if (!emailValidation.test(email.value)) {
        warnings += `El email no es valido </br>`;
        entrar = true;
    }

    if (textarea.value.length == 0 ) {
        warnings += `El textarea está vacio </br>`;
        entrar = true;
    }

    if (entrar) {
        parrafo.innerHTML = warnings;
    } else {
        parrafo.innerHTML = "Envio correcto";
    }

});

function limita(elEvento, maximoCaracteres) {
    var elemento = document.getElementById("texto");

    // Obtener la tecla pulsada
    var evento = elEvento || window.event;
    var codigoCaracter = evento.charCode || evento.keyCode;
    // Permitir utilizar las teclas con flecha horizontal
    if (codigoCaracter == 37 || codigoCaracter == 39) {
        return true;
    }

    // Permitir borrar con la tecla Backspace y con la tecla Supr.
    if (codigoCaracter == 8 || codigoCaracter == 46) {
        return true;
    } else if (elemento.value.length >= maximoCaracteres) {
        return false;
    } else {
        return true;
    }
}

function cuentaAtras(maximoCaracteres) {
    var elemento = document.getElementById("texto");
    var info = document.getElementById("info");

    if (elemento.value.length >= maximoCaracteres) {
        info.innerHTML = "Máximo " + maximoCaracteres + " caracteres";
    } else {
        info.innerHTML = "Puedes escribir hasta " + (maximoCaracteres - elemento.value.length) + " caracteres adicionales";
    }
}
