window.addEventListener('load', createClock);

function createClock() {
    setInterval(() => {
        let myDate = new Date();
        let hours = myDate.getHours();
        let minutes = myDate.getMinutes();
        let secodns = myDate.getSeconds();

        let clock = document.getElementById('clock');
        clock.innerHTML = hours + ": " + minutes + ": " + secodns;
    }, 1000);
}