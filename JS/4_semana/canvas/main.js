var SCENE;
var CONTEXT;
var FPS = 50;
window.addEventListener('load', main);

function main() {

    var hero = new mainCharacter(0, 0);
    var foeList = [
        foe1 = new foe(50, 0),
        foe2 = new foe(100, 100)
    ];


    setInterval(() => {
        SCENE = document.getElementById('scene');
        CONTEXT = SCENE.getContext("2d");
        cleanScene();
        hero.draw();
        for (let i = 0; i < foeList.length; i++) {
            foeList[i].move();
            foeList[i].draw();
        }
        /* Al crear un array no necesitamos llamarlos uno a uno */
        // foe1.move();
        // foe2.move();
        // foe1.draw();
        // foe2.draw();
    }, 1000 / FPS);



}

function cleanScene() {
    SCENE.width = SCENE.width;
    SCENE.height = SCENE.height;
}


// Fumncion Constructora protagonista

function mainCharacter(coordX, coordY) {
    this.coordX = coordX;
    this.coordY = coordY;

    document.addEventListener("keydown", (event) => this.move(event));

    this.draw = function () {
        CONTEXT.fillStyle = "#FF0000";
        CONTEXT.fillRect(this.coordX, this.coordY, 32, 32);
    };

    this.move = function (event) {
        CONTEXT.fillStyle = "black";
        CONTEXT.fillRect(this.coordX, this.coordY, 32, 32);

        var keyCode = event.which || event.keyCode;

        switch (keyCode) {
            case 37: //  left
                if (this.coordX > 0) {
                    this.coordX -= 10;
                }
                break;
            case 38: // UP
                if (this.coordY > 0)
                    this.coordY -= 10;
                break;
            case 39: //  Right
                if (this.coordX + 32 < SCENE.width) {
                    this.coordX += 10;
                }

                break;
            case 40: //  Down
                if (this.coordY + 32 < SCENE.height) {
                    this.coordY += 10;
                }

                break;
        }
    };
}

// Fumncion Constructora malos
function foe(coordX, coordY) {
    this.coordX = coordX;
    this.coordY = coordY;

    document.addEventListener("keydown", (event) => this.move(event));

    this.draw = function () {
        CONTEXT.fillStyle = "#0000FF";
        CONTEXT.fillRect(this.coordX, this.coordY, 32, 32);
    };

    this.move = function (event) {
        //numero aleatorio
        var direction = Math.floor(Math.random() * 4);
        switch (direction) {
            case 0: //  left
                if (this.coordX > 0) {
                    this.coordX -= 10;
                }
                break;
            case 1: // UP
                if (this.coordY > 0)
                    this.coordY -= 10;
                break;
            case 2: //  Right
                if (this.coordX + 32 < SCENE.width) {
                    this.coordX += 10;
                }

                break;
            case 3: //  Down
                if (this.coordY + 32 < SCENE.height) {
                    this.coordY += 10;
                }

                break;
        }
    };
}

function checkSceneLimits(newCoodX, newCoordY) {
    var width = SCENE.width;
    var height = SCENE.height;

    return newCoodX > width || newCoordY > height;
}