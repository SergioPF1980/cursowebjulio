window.addEventListener('load', ()=>{
    document.myForm.addEventListener('submit', validateForm);
});

function validateForm(event) {
    event.preventDefault();
    var regExpEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    // formamos un objeto con los nombres de los campos del formulario
    var objForm = {
        fav_language: requiredValue('fav_language'),
        colors: requiredValue('colors'),
        description: requiredValue('description'),
        email: requiredFormat('email', regExpEmail) ,
        name: requiredValue('name')
    };

    let errorsForm = treatErrorsForm(objForm);
    if(errorsForm.length > 0) {
        alert(errorsForm);
        return false;
    }
    return true;
}

function treatErrorsForm(objectFormValues) {
    var errors = [];
    var keysArray = Object.keys(objectFormValues);
    for(var i = 0; i < keysArray.length; i++) {
        let key = keysArray[i];
        if(objectFormValues[key].error){
            errors.push(objectFormValues[key].error);
        }
    }
    return errors;
}

function requiredFormat(field, regExpression) {
    try{
        var resultFormat = {};
        var fieldValue = document.myForm[field].value;
        if (regExpression.test(fieldValue)) {
            resultFormat.validation = true;
            return resultFormat;
        }else {
            return {
                validation: false,
                error: 'El campo ' + field + ' no tiene un formato correcto'
            };
        }
    }catch(exceptionError){
        return {
            validation: false,
            error: exceptionError.message
        }
    }
}

function requiredValue(field) {
    try{
        var resultValue = {};
        console.log(document.myForm[field]);
        var fieldValue = document.myForm[field].value.trim();
        if(fieldValue){
            resultValue.validation = true;
            return resultValue;
        } 
        return {
            validation: false,
            error: 'El campo ' + field + ' es obligatorio'
        }
    }catch(exceptionError){
        return {
            validation: false,
            error: exceptionError.message
        }
    }
}





