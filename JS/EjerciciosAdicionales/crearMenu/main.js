window.addEventListener("load", createMenu);

function createMenu() {
    // Obtener todos los elementos h2
    var h2s = document.getElementsByTagName('h2');
    // Crear nuevo elemento div para añadir el menu
    var menu = document.createElement('div');
    // Crear nuevo elemento ul para añadir en el menu
    var menuUl = document.createElement('ul');

    // Añadimos ul al div
    menu.appendChild(menuUl);

    // mientras hay h2
    for (let i = 0; i < h2s.length; i++) {
        // obtener el nodo de tecxto de las cabeceras
        var itemText = h2s[i].childNodes[0].nodeValue;
        // cre nuevo elemento de la listas
        var menuLi = document.createElement('li');
        menuUl.appendChild(menuLi);
        // Añadir enlace ala lista
        var menuLiA = document.createElement('a');
        menuLiA = menuLi.appendChild(menuLiA);

        // Añadir href del enlace(#item)
        menuLiA.setAttribute("href", "#item" + i);

        // Añadir texto del enlaces
        var menuText = document.createTextNode(itemText);
        menuLiA.appendChild(menuText);

        // Añadir el atributo name asociado con en el enlaces
        var anc = document.createElement('a');
        anc.setAttribute('name', 'item' + i);

        // Añadirlo antes de la cabeceras
        document.body.insertBefore(anc, h2s[i]);
    }

    document.body.insertBefore(menu, document.body.firstChild);
}