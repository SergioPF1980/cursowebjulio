// windows.addEventListener('load', () => {
//     document.myForm.addEventListener('onchange', loadComunidad);
// });

window.addEventListener("load", init());

function init() {


    let options = [{
            value: "value1",
            text: "opcion 1"
        },
        {
            value: "value2",
            text: "opcion 2"
        },
        {
            value: "value3",
            text: "opcion 3"
        }
    ];



    let select = createSELECT(options);

    document.body.appendChild(select);

}

function createSELECT(options) {

    let select = document.createElement("select");
    if (options && Array.isArray(options)) {
        for (let index = 0; index < options.length; index++) {
            const element = options[index];

            let option = document.createElement("option");

            if (element.value) {
                option.setAttribute("value", element.value);
            }
            if (element.text) {
                let optionText = document.createTextNode(element.text);
                option.appendChild(optionText);
            }

            select.appendChild(option);
        }
    }

    return select;
}
