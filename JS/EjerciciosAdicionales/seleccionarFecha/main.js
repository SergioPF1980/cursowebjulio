Calendar.setup({
    inputField: "fecha",
    ifFormat: "%d/%m/%Y",
    weekNumbers: false,
    displayArea: "fecha_usuario",
    daFormat: "%A, %d de %B de %Y"
});