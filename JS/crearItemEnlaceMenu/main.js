window.addEventListener('load', init);
const lista = document.getElementById('lista');
// console.log(lista);



const arrayItem = ['Item 1', 'Item 2', 'Item 3'];

/* Primera Forma */
// arrayItem.forEach(item => {
//     console.log(item);
//     const li = document.createElement('li');
//     li.textContent = item;

//     lista.appendChild(li);
// });

/* Segunda Forma */
// arrayItem.forEach(item =>{
//     lista.innerHTML += `<li>${item}</li>`;
// })

/*** Puede dar problemas por el Reflow ***/


/* Tercera Forma sin Reflow */

const fragment = document.createDocumentFragment();
var i = 0;
arrayItem.forEach(item => {

    const li = document.createElement('li');
    // li.textContent = item;
    const a = document.createElement('a');
    a.href = '#' + arrayItem[i];
    a.innerHTML = arrayItem[i];
    li.appendChild(a);
    fragment.appendChild(li);
    i++;
});

lista.appendChild(fragment);

function init(){
    var submenu = document.getElementById('submenu');
    var item = document.getElementsByTagName('tag');
    
}