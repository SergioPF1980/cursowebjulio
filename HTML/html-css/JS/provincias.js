window.addEventListener('load', () => {
    document.myForm.addEventListener('load', loadCominidad);
});

function loadCominidad() {
    var arrComunidades = ['Cantabria', 'Castilla La Mancha', 'Comunidad Valenciana'];
    arrComunidades.sort();
    addOptions('provincia', arrComunidades);
}


function addOptions(element, array) {
    var selector = document.getElementsByName(element)[0];
    for (comunidad in array) {
        var option = document.createElement('option');
        option.text = array[comunidad];
        option.value = array[comunidad].toLowerCase();
        selector.add(option);
    }
}

function loadProvincias() {
    var listProvincias = {
        cantabria: ['lkjlkj'],
        castilla: ['Albacete', 'Cuenca'],
        valencia: ['Valencia']
    }

    var comunidades = document.getElementById('comunidad');
    var provincias = document.getElementById('provincia');
    var comunidadSelect = comunidades.value;

    provincias.innerHTML ='<option value="">Seleccione una Provincia</option>';

    if(comunidadSelect!== ''){
        comunidadSelect = listProvincias[comunidadSelect];
        comunidadSelect.sort();

        comunidadSelect.array.forEach(function(provincia) {
            let option = document.createElement('option');
            option.value = provincia;
            option.text = provincia;
            provincia.add(option);
        });
    }
}
