window.addEventListener('load', init);

function init() {
    var menu = document.getElementById('menu');
    var activeItem = menu.getElementsByTagName('a');

    for (let i = 0; i < activeItem.length; i++) {
        let items = activeItem[i];
        items.addEventListener('click', () => {

            // alert('hasta aqui');

            for (let i = 0; i < activeItem.length; i++) {
                let items = activeItem[i];
                items.classList.remove('active');
            }


            items.classList.add('active');
        });
    }
    addImg();
    createClock();
}

function addImg() {
    var img = document.getElementById('image');
    var imagen = document.createElement("img");

    imagen.src = "./html5Black.png";
    // imagen.height = "50";
    imagen.width = "100";
    imagen.verticalAlign = "middle";
    // margin-left: 50px;
    // var src = document.getElementById("imagen");
    img.appendChild(imagen);

}

function createClock() {
    setInterval(() => {
        let myDate = new Date();
        let hours = myDate.getHours();
        let minutes = myDate.getMinutes();
        let secodns = myDate.getSeconds();

        let clock = document.getElementById('clock');
        clock.innerHTML = hours + ": " + minutes + ": " + secodns;
    }, 1000);


}