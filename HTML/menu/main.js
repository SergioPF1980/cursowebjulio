const btnmenu = document.getElementById('btnmenu');
const menu = document.getElementById('menu');

btnmenu.addEventListener('click', function () {
    menu.classList.toggle('mostrar'); //  Añado la clase mostrar
});

const subnmenuBtn = document.getElementsByClassName('submenu-btn');
// const subnmenuBtn = document.querySelectorAll('.submenu-btn');

for (let i = 0; i < subnmenuBtn.length; i++) {
    subnmenuBtn[i].addEventListener('click', function () {
        if(window.innerWidth < 1024){
            var submenu = this.nextElementSibling;
            var height = submenu.scrollHeight;
            if(submenu.classList.contains('desplegar')){
                submenu.classList.remove('desplegar');
                submenu.removeAttribute('style');
            }else{
                submenu.classList.add('desplegar');
                submenu.style.height = height + 'px';
            }
        }
    });
}